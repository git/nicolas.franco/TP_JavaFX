package view;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.CapteurAbstrait;
import model.StrategyRandom;

import java.io.IOException;
import java.io.InputStream;

public class ImageCtrlView extends CapteurView{
    private final static String fxmlfile = "view/Image.fxml";
    Class<?> clazz = this.getClass();
    InputStream input;

    protected CapteurAbstrait capteur;
    @FXML
    ImageView imageview;

    public ImageCtrlView(CapteurAbstrait capteur, String url, String title) throws IOException {
        super(capteur,url,title);
        this.capteur = capteur;
        myBtn.setOnAction(e -> Platform.exit());
        changeImage();
    }

    public void changeImage() throws IOException {
        if (this.capteur.getTemp().getValue() > 25){
            input = clazz.getResourceAsStream("/images/sun.png");
            imageview.setImage(new Image(input));
        } else if (this.capteur.getTemp().getValue() > 10) {
            input = clazz.getResourceAsStream("/images/suncloud.png");
            imageview.setImage(new Image(input));
        }
        else{
            input = clazz.getResourceAsStream("/images/snow.png");
            imageview.setImage(new Image(input));
        }
        imageview.setFitHeight(100);
        imageview.setFitWidth(100);
    }

    private void quit(){
        Platform.exit();
    }

    public void initialize(CapteurAbstrait capteur) {
        /*capteur.getTemp().addListener((observable, oldValue, newValue) -> {
            try {
                changeImage();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });*/
    }
}
