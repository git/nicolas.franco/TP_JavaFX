package view;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import model.CapteurAbstrait;
import model.StrategyRandom;

import java.io.IOException;

public class ThermostatView extends CapteurView{
    private final static String fxmlfile = "view/Thermostat.fxml";

    @FXML
    Spinner<Double> spin;

    protected CapteurAbstrait capteur;

    public ThermostatView(CapteurAbstrait capteur, String url, String title) throws IOException {
        super(capteur,url,title);
        this.capteur=capteur;
        this.spin.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0d,100d));
        this.spin.getValueFactory().valueProperty().bindBidirectional(this.capteur.getTemp());
        myBtn.setOnAction(e -> Platform.exit());

    }

    public void initialize() {
    }

}
