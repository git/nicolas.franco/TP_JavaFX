package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import model.CapteurAbstrait;
import model.StrategyRandom;

import java.io.IOException;


public abstract class CapteurView extends FXMLView {
    @FXML
    Button myBtn;

    protected CapteurAbstrait capteur;

    public CapteurView(CapteurAbstrait capteur, String url, String title) throws IOException {
        super(url,title);
        this.setCapteur(capteur);
    }

    public void setCapteur(CapteurAbstrait capteur) {
        this.capteur = capteur;
    }

}
