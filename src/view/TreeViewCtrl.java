package view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.NumberStringConverter;
import model.*;

import javax.swing.text.LabelView;

public class TreeViewCtrl extends FXMLView {
    @FXML
    private TreeView<CapteurAbstrait> treeView;

    @FXML
    private Label idLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private TextField nameTextField;
    @FXML
    private Label temperatureLabel;

    @FXML private TableView sousCapteurs;
    @FXML
    private ComboBox strategyOptions;
    @FXML
    private ToggleButton arretGenTemp;

    private ArrayList<CapteurAbstrait> capteurAbstraits;


    public TreeViewCtrl(ArrayList<CapteurAbstrait> capteurs, String url, String title) throws IOException {
        super(url,title);
        this.capteurAbstraits = capteurs;

        TreeItem<CapteurAbstrait> root = new TreeItem<>();
        treeView.setShowRoot(false);
        root.setExpanded(true);
        //treeView.setRoot(root);
        TreeItemFactoryVisitor treeItemFactoryVisitor = new TreeItemFactoryVisitor();

        treeView.setVisible(true);

        if (root != null && treeView != null) {
            treeView.setRoot(root);
            for (CapteurAbstrait capteur : capteurAbstraits) {
                TreeItem<CapteurAbstrait> item = capteur.accept(treeItemFactoryVisitor);
                root.getChildren().add(item);
            }
        }
//        for(CapteurAbstrait capteur: capteurAbstraits){
//            TreeItem<CapteurAbstrait> item = capteur.accept(treeItemFactoryVisitor);
//            root.getChildren().add(item);
//        }
        this.initializeCapteur();

    }

    public void initializeCapteur() {
        treeView.setCellFactory(param -> new TreeCell<CapteurAbstrait>() {
            @Override
            protected void updateItem(CapteurAbstrait capteur, boolean empty) {
                super.updateItem(capteur, empty);
                if (empty || capteur == null) {
                    textProperty().unbind();
                    setText("");
                } else {
                    setText(capteur.getNom().getValue());
                    String image = "/images/random.png";
                    if (capteur.getClass() == CapteurVirtuel.class) {
                        image = "/images/virtual.png";
                    } else if (capteur.getClass() == UnitCapteur.class && ((UnitCapteur) capteur).getStrategie().getClass() == StrategyCPU.class) {
                        image = "/images/cpu.png";
                    } else if (capteur.getClass() == UnitCapteur.class && ((UnitCapteur) capteur).getStrategie().getClass() == StrategyRandom.class) {
                        image = "/images/random.png";
                    }

                    setGraphic(new ImageView(new Image(image)) {{
                        setFitHeight(40);
                        setFitWidth(40);
                    }});
                }
            }
        });
    }

    public void initialize(){
        strategyOptions.setVisible(false);
        sousCapteurs.setVisible(false);
        arretGenTemp.setVisible(false);
        treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null) {
                CapteurAbstrait capteur = oldValue.getValue();
                idLabel.textProperty().unbindBidirectional(capteur.getId());
                nameLabel.textProperty().unbindBidirectional(capteur.getNom());
                temperatureLabel.textProperty().unbind();
                if(capteur instanceof UnitCapteur){
                    strategyOptions.setVisible(false);
                }
                sousCapteurs.getItems().clear();
                sousCapteurs.getColumns().clear();
            }
            if (newValue != null) {
                CapteurAbstrait capteur = newValue.getValue();
                if (capteur instanceof UnitCapteur) {
                    strategyOptions.setVisible(true);
                } else {
                    strategyOptions.setVisible(false);
                }
                idLabel.textProperty().bindBidirectional(capteur.getId());
                nameLabel.textProperty().bindBidirectional(capteur.getNom());
                temperatureLabel.textProperty().bind(Bindings.format("%.2f",capteur.getTemp()));
                nameTextField.setOnAction(event -> {
                    capteur.setNom(nameTextField.getText());
                });
                nameTextField.setText(capteur.getNom().getValue());
                capteur.getNom().addListener((obs, oldName, newName) -> {
                    capteur.setNom(newName);
                    newValue.setValue(capteur);
                });
                if(capteur instanceof UnitCapteur){
                    strategyOptions.setVisible(true);
                    sousCapteurs.setVisible(false);
                    arretGenTemp.setVisible(true);
                    strategyOptions.setOnAction(event ->{
                        if(strategyOptions.getValue().equals("CPU")){
                            ((UnitCapteur) newValue.getValue()).setStrategy(new StrategyCPU());
                        }
                        else if (strategyOptions.getValue().equals("Random")){
                            ((UnitCapteur) newValue.getValue()).setStrategy(new StrategyRandom());
                        }
                        if(!newValue.getValue().isGenTemp()){
                            arretGenTemp.setSelected(true);
                        };
                        initializeCapteur();
                    });
                    arretGenTemp.setOnAction(event -> {
                        newValue.getValue().setIsGenTemp(!arretGenTemp.isSelected());
                    });
                } else if(capteur instanceof CapteurVirtuel){
                    strategyOptions.setVisible(false);
                    sousCapteurs.setVisible(true);
                    arretGenTemp.setVisible(false);
                    ObservableList<CapteurAbstrait> capteurs = FXCollections.observableArrayList(newValue.getValue().getCapteurs().keySet());
                    sousCapteurs.setItems(capteurs);
                    TableColumn<CapteurAbstrait, Object> iconColumn = new TableColumn<>("Icone");
                    iconColumn.setCellFactory(column -> {
                        return new TableCell<CapteurAbstrait, Object>() {
                            @Override
                            protected void updateItem(Object item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                } else {
                                    if (getTableRow() != null){
                                        CapteurAbstrait capteur = (CapteurAbstrait) getTableRow().getItem();
                                        if (capteur instanceof CapteurVirtuel) {
                                            ImageView capVirt = new ImageView("images/virtual.png");
                                            capVirt.setFitWidth(25);
                                            capVirt.setFitHeight(25);
                                            setGraphic(capVirt);
                                        } else if (capteur instanceof UnitCapteur) {
                                            UnitCapteur unitCaptor = (UnitCapteur) capteur;
                                            if (unitCaptor.getStrategie() instanceof StrategyCPU) {
                                                ImageView capCpu = new ImageView("images/cpu.png");
                                                capCpu.setFitWidth(25);
                                                capCpu.setFitHeight(25);
                                                setGraphic(capCpu);
                                            } else if (unitCaptor.getStrategie() instanceof StrategyRandom) {
                                                ImageView capRand = new ImageView("images/random.png");
                                                capRand.setFitWidth(25);
                                                capRand.setFitHeight(25);
                                                setGraphic(capRand);
                                            }
                                        }
                                    }
                                }
                            }
                        };
                    });
                    sousCapteurs.getColumns().add(iconColumn);
                    TableColumn<CapteurAbstrait, Integer> idColumn = new TableColumn<>("id");
                    idColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(Integer.parseInt(cellData.getValue().getId().getValue())).asObject());
                    sousCapteurs.getColumns().add(idColumn);
                    TableColumn<CapteurAbstrait, String> NameColumn = new TableColumn<>("Nom");
                    NameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom().getValue()));
                    sousCapteurs.getColumns().add(NameColumn);
                    NameColumn.setEditable(true);

                    TableColumn<CapteurAbstrait, Integer> poidsColumn = new TableColumn<>("Poids");
                    poidsColumn.setCellValueFactory(cellData -> {
                        CapteurAbstrait captor = cellData.getValue();
                        if (captor != null && newValue.getValue().getCapteurs().get(captor) != null) {
                            return new SimpleIntegerProperty(newValue.getValue().getCapteurs().get(captor)).asObject();
                        } else {
                            return new SimpleObjectProperty<>(null);
                        }
                    });
                    sousCapteurs.getColumns().add(poidsColumn);
                    poidsColumn.setEditable(true);
                    poidsColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

                    poidsColumn.setOnEditCommit(event -> {
                        CapteurAbstrait capteur2 = event.getRowValue();
                        newValue.getValue().getCapteurs().put(capteur2, event.getNewValue());
                    });
                }
            }
        });
    }

    public TreeView<CapteurAbstrait> getTreeView() {
        return treeView;
    }
}
