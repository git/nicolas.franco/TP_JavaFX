package model;

import javafx.beans.property.*;

import java.util.HashMap;
import java.util.Map;

public abstract class CapteurAbstrait {
    private Property<String> id;
    private StringProperty nom;
    private StringProperty displayNom;
    private ObjectProperty<Double> temp = new SimpleObjectProperty<Double>();
    private Boolean isGenTemp;

    CapteurAbstrait(Property<String> id, StringProperty nom){
        this.id = id;
        this.nom = nom;
        this.displayNom = new SimpleStringProperty();
        this.displayNom.bind(this.nom);
        this.temp.setValue(0.0);
        this.isGenTemp = true;
    }
    public abstract void genTemp();

    /* Getters - Setters */
    public Property<String> getId() {
        return id;
    }

    public void setId(Property<String> id) {
        this.id = id;
    }

    public StringProperty getNom() {
        return this.nom;
    }

    public StringProperty getDisplayNom() {
        return this.displayNom;
    }

    public void setNom(StringProperty nom) {
        this.nom = nom;
    }
    public void setNom(String nom) {
        this.nom.setValue(nom);
    }

    public ObjectProperty<Double> getTemp() {
        return temp;
    }

    public void setTemp(double pTemp) {
        this.temp.setValue(pTemp);
    }

    public abstract <T> T accept (Visitor<T> v);

    public Map<CapteurAbstrait, Integer> getCapteurs(){
        return new HashMap<>();
    };

    public Boolean isGenTemp() {return this.isGenTemp;};
    public void setIsGenTemp(boolean b) {this.isGenTemp = b;};
}
