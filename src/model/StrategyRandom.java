package model;


import java.util.Random;

public class StrategyRandom implements StrategyCaptor {

    @Override
    public  double genTemp() {
        Random random = new Random();
        return random.nextDouble(30) - 30;
    }
}
