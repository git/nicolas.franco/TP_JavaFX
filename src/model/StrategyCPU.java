package model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class StrategyCPU implements StrategyCaptor {

    @Override
    public double genTemp() {
        try{
            String temp = new String(Files.readAllBytes(Paths.get("/sys/class/thermal/thermal_zone0/temp")));
            double tempCelsius = Double.parseDouble(temp)/1000.0;
            return tempCelsius;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
