package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CapteurVirtuel extends CapteurAbstrait {

    private Map<CapteurAbstrait, Integer> capteurs;

    public CapteurVirtuel(Property<String> id, StringProperty nom){
        super(id, nom);
        this.capteurs = new HashMap<>();
    }
    public void addCapteur(UnitCapteur c, int p){
        this.capteurs.put(c,p);
    }

   public void removeCapteur(UnitCapteur c){
        this.capteurs.remove(c);
    }

    public <T> T accept(Visitor<T> v){
        return v.visit(this);
    }

    public Map<CapteurAbstrait, Integer> getCapteurs(){return this.capteurs;}

    // calcule la temperature moyenne des capteurs sa collection de capteurs
    public void genTemp() {
        double sum = 0;
        int count = 0;
        for (Map.Entry<CapteurAbstrait, Integer> entry : capteurs.entrySet()) {
            CapteurAbstrait c = entry.getKey();
            int p = entry.getValue();
            sum += c.getTemp().getValue() * p;
            count += p;
        }
        double avg = sum / count;
        this.setTemp(avg);
    }
}
