package model;

import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;

import java.time.temporal.ChronoUnit;

public class UnitCapteur extends CapteurAbstrait{
    private StrategyCaptor strategie;

    public UnitCapteur(Property<String> id, StringProperty nom, StrategyCaptor st) {
        super(id, nom);
        this.strategie = st;
    }

    @Override
    public void genTemp() {
        this.setTemp(strategie.genTemp());
    }

    @Override
    public <T> T accept(Visitor<T> v) {
        return v.visit(this);
    }

    public StrategyCaptor getStrategie() {
        return strategie;
    }

    public void setStrategy(StrategyCaptor strategie) {
        this.strategie = strategie;
    }
}
