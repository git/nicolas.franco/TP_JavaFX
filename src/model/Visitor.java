package model;

public interface Visitor<T> {
    public T visit(UnitCapteur c);
    public T visit(CapteurVirtuel cv);
}
