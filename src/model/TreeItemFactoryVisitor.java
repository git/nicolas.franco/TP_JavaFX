package model;

import com.sun.source.tree.Tree;
import javafx.scene.control.TreeItem;

public class TreeItemFactoryVisitor implements Visitor<TreeItem<CapteurAbstrait>>{
    @Override
    public TreeItem<CapteurAbstrait> visit(UnitCapteur c) {
        return new TreeItem<>(c);
    }

    @Override
    public TreeItem<CapteurAbstrait> visit(CapteurVirtuel cv) {
        TreeItem<CapteurAbstrait> root = new TreeItem<>(cv);
        root.setExpanded(true);

        for(CapteurAbstrait c : cv.getCapteurs().keySet()){
            TreeItem<CapteurAbstrait> item = new TreeItem<>(c);
            if(c.getClass() == CapteurVirtuel.class){
                item = c.accept(this);
            } else {
                item = new TreeItem<>(c);
            }
            root.getChildren().add(item);
        }
        return root;
    }
}
