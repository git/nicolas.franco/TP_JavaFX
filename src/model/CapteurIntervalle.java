/*
GENERATE PARAMETER STRATEGY
package model;

import javafx.beans.property.ObjectProperty;

import java.util.Random;

public class CapteurIntervalle extends CapteurAbstrait{

    Un capteur peut générer une température dont la valeur est aléatoire;
    Les températures ne pouvant descendre sous un certain seuil, on veut
    aussi un capteur plus réaliste qui génère des températures dont la valeur
    est comprise dans un certain intervalle défini à la création (et dont le
    minimum ne peut être inférieur à 0°K);

    public CapteurIntervalle(int id, String nom, Double minVal, Double maxVal){
        super(id,nom);
        // si min inférieur a zero, alors passe a 0
        if(minVal < 0){
            minVal = (double) 0;
        }
        this.minVal = minVal;
        this.maxVal = maxVal;
    }

    private Double minVal;
    private Double maxVal;
    @Override
    public void genTemp() {
        Random rand = new Random();
        double randomValue = rand.nextDouble(this.maxVal - this.minVal) + minVal;
        this.setTemp(randomValue);
    }
}
*/
