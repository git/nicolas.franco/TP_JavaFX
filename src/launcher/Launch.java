package launcher;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TreeItem;
import javafx.stage.Stage;
import model.*;
import view.CapteurView;
import view.ImageCtrlView;
import view.ThermostatView;
import view.TreeViewCtrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Launch extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException, InterruptedException {
        // Creation des capteurs
        UnitCapteur captorC = new UnitCapteur(new SimpleStringProperty("1"),new SimpleStringProperty("Capteur CPU"),new StrategyCPU());
        UnitCapteur captorR = new UnitCapteur(new SimpleStringProperty("2"),new SimpleStringProperty("Capteur Random"),new StrategyRandom());
        CapteurVirtuel captorV = new CapteurVirtuel(new SimpleStringProperty("3"),new SimpleStringProperty("Capteur Virtuel"));

        // Ajout des capteurs dans la collections du capteur virtuel
        captorV.addCapteur(captorC,1);
        captorV.addCapteur(captorR,1);
        for (Map.Entry<CapteurAbstrait, Integer> entry : captorV.getCapteurs().entrySet()) {
            CapteurAbstrait c = entry.getKey();
            c.genTemp();
        }
        captorV.genTemp();
        ArrayList<CapteurAbstrait> capteurs = new ArrayList<>();
        capteurs.add(captorC);
        capteurs.add(captorR);
        capteurs.add(captorV);

        Stage thermostatStage = new Stage();
        TreeViewCtrl treeViewCtrl = new TreeViewCtrl(capteurs,"view/TreeView.fxml","treeView");
        Thread t = new Thread() {
            public void run() {
                while (true) {
                    Platform.runLater(() -> {
                        capteurs.forEach(c -> {
                            if(c instanceof UnitCapteur uc && uc.isGenTemp()){
                                uc.genTemp();
                            }
                        });
                        capteurs.forEach(c -> {
                            if(c instanceof CapteurVirtuel vc){
                                vc.genTemp();
                            }});
                    });
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };
        t.start();
        treeViewCtrl.show();
    }
}

